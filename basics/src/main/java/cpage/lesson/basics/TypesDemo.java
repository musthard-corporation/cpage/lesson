package cpage.lesson.basics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

@SuppressWarnings("unused")

/* Objet */
public class TypesDemo {
	
	/**
	 * Javadoc de function
	 */
	public void function() {
		/* Chaîne de caractères */
		String chars = "@a1-.";
		
		/* Tableau */
		int[] intArray = new int[5];
		
		/* Liste ordonnée */
		List<Integer> intList = new ArrayList<Integer>();
		
		/* Liste sans élément dupliqué */
		Set<Integer> intSet = new HashSet<Integer>();
		
		/* Dictionnaire */
		Map<Integer, String> intMap = new HashMap<Integer, String>();		
		
		/* Enumération */
		IntEnum value = IntEnum.Zero;
	}	
}

/* Enumération */
enum IntEnum {
	Zero,
	One,
	Two
}
