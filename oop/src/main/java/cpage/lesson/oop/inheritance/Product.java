package cpage.lesson.oop.inheritance;

public abstract class Product implements IProduct {
	public float price;
	public String model;
	public String color;
	
	public Product(float price, String model, String color) {
		this.price = price;
		this.model = model;
		this.color = color;
	}

	public float computeDiscount(int percentage) {
		return this.price * (1 - percentage / 100);
	}
	
	public abstract String[] getAvailableColors();
}
