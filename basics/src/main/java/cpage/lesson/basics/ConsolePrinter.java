package cpage.lesson.basics;

import java.util.Scanner;

public class ConsolePrinter {
	private static Scanner scanner = new Scanner(System.in);
	
	public void print() {
		// Affiche dans la console sans retour à la ligne
		System.out.print("");
		
		// Affiche dans la console avec un retour à la ligne à la fin
		System.out.println("");
		
		// Affiche dans la console en utilisant le même fonctionnement que String.format("") sans retour à la ligne
		/*
		 * %s : string
		 * %d : short / int / long
		 * %f : float / double
		 * %t : date
		 * %n : retour à la ligne
		 * 
		 * Plus de possibilité : https://docs.oracle.com/javase/8/docs/api/java/util/Formatter.html
		 */
		System.out.printf("");
	}
	
	public void prompt() {		
		// Si l'utilisateur renseigne : "hello world\n"
		
		// On récupère : "hello"
		scanner.next();
		
		// On récupère : "hello world\n"
		scanner.nextLine();
	}
}
