package cpage.unitTesting;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.*;

import org.junit.jupiter.api.*;

public class ClassUnderTest {
	@BeforeAll
	static void setup() {
		System.out.println("I'm executed only one time : before all test methods execution in this test class");
	}

	@AfterAll
	static void cleanup() {
		System.out.println("I'm executed only one time : after all test methods execution in this test class");
	}

	@BeforeEach
	void init() {
		System.out.println("I'm executed multiple time : before each test methods execution in this test class");
	}
	
	@AfterEach
	void tearDown() {
		System.out.println("I'm executed multiple time : after each test methods execution in this test class");
	}
	
	@Test
	@DisplayName("Optional : Name displayed in test result instead of method name")
	@Disabled("Optional : Explain why it is disabled")
	void example() {}
	
	@Test
	void testFunctionName() {
		// Always expected value first, and value to test after
		// It is possible to put a message in the begin of the function call
		assertEquals(1, 1);
		assertTrue(true);
		assertFalse(false);
		assertNull(null);
		assertAll(
				() -> assertTrue(false), 
				() -> {
					String expected = "hello";
					String actual = "HellO";
					assertEquals(expected, actual);
				}
		);
		assertThrows(Exception.class, () -> { throw new Exception(); });
		
		// All assertion above can be negative 
		// (assertNotEquals, assertNotNull, assertDoesNotThrow, ...)
	}
	
	@Test
	@Disabled("Optional : Explain why it is disabled")
	void testDisabled() {
	}
	
	@Test
	void testExecutedOnlyIfTrueConditionMeet() {
		assumeTrue(false);
		assertEquals("Whoopee I can run", "");
	}
	
	@Test
	void testExecutedOnlyIfFalseConditionMeet() {
		assumeFalse(true);
		assertEquals("Whoopee I can run", "");
	}
	
	@Test
	void testExecutedOnlyIfConditionMeet() {
		assumingThat(false, () -> assertEquals("Whoopee I can run", ""));
	}
}
