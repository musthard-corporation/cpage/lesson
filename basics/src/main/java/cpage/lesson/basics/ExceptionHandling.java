package cpage.lesson.basics;

import java.io.IOException;

public class ExceptionHandling {
	// https://programming.guide/java/list-of-java-exceptions.html
	
	public void signedCatchExample() {
		try {
			signedThrowsExample();
		} catch (IOException e) {
			// probleme avec un fichier
		} catch (Exception e) {
			// une erreur inattendue est survenue
		}
	}

	public void signedThrowsExample() throws Exception {
		throw new Exception();
	}

	public void unsignedExample() {
		Object objet = null;
//		throw new NullPointerException();
		objet.toString();
	}
}
