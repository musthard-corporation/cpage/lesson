package cpage.lesson.oop;

import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;

public class IdentityCard {
	private static final int AVAILABILITY_DURATION_IN_YEAR = 10;

	String documentNumber;
	String surname;
	String[] givenNames;
	char sex;
	String nationality;
	Calendar birthDate;
	String birthPlace;
	String address;
	float height;
	Calendar issueDate;

	public IdentityCard(String documentNumber, String surname, String[] givenNames, char sex, String nationality,
			Calendar birthDate, String birthPlace, String address, float height, Calendar issueDate) {
		this.documentNumber = documentNumber;
		this.surname = surname;
		this.givenNames = givenNames;
		this.sex = sex;
		this.nationality = nationality;
		this.birthDate = birthDate;
		this.birthPlace = birthPlace;
		this.address = address;
		this.height = height;
		this.issueDate = issueDate == null ? Calendar.getInstance() : issueDate;
	}

	public boolean isMan() {
		// TODO - Char depends on where the card is made (H in France, M in England,
		// ...)
		// Best practice : Create an enum to not depend on country

		return this.sex == 'H';
	}

	public boolean isWoman() {
		// TODO - Char depends on where the card is made (H in France, M in England,
		// ...)
		// Best practice : Create an enum to not depend on country

		return this.sex == 'F';
	}

	public int getAge() {
		LocalDate birth = this.birthDate.toInstant().atZone(this.birthDate.getTimeZone().toZoneId()).toLocalDate();
		return Period.between(birth, LocalDate.now()).getYears();
	}

	public Calendar getExpirationDate() {
		Calendar expirationDate = (Calendar) this.issueDate.clone();
		expirationDate.add(Calendar.YEAR, AVAILABILITY_DURATION_IN_YEAR);
		return expirationDate;
	}

	public boolean isExpired() {
		Calendar now = Calendar.getInstance();
		return now.after(this.getExpirationDate());
	}
}
