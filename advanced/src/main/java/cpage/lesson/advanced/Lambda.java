package cpage.lesson.advanced;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Lambda {
	void exampleArray(String[] array) {
		Arrays.stream(array).forEach((row) -> System.out.println(row));

		for (String row : array) {
			System.out.println(row);
		}
	}

	void exampleList(List<String> list) {
		list.stream().forEach(null);

		boolean isSomeStringEmpty = list.stream().anyMatch((row) -> row.isEmpty());
		boolean isAllStringEmpty = list.stream().allMatch((row) -> row.isEmpty());

		isSomeStringEmpty = false;
		for (String row : list) {
			if (row.isEmpty()) {
				isSomeStringEmpty = true;
			}
		}

		boolean isSomeStringNotEmpty = false;
		for (String row : list) {
			if (!row.isEmpty()) {
				isSomeStringNotEmpty = true;
			}
		}
		isAllStringEmpty = !isSomeStringNotEmpty;
		
		List<String> filteredList = list.stream().filter((row) -> row.isEmpty()).collect(Collectors.toList());

		// "a".compareTo("b") == -1
		// "a".compareTo("a") == 0
		// "b".compareTo("a") == 1
		List<String> sortedList = list.stream().sorted((a, b) -> a.compareTo(b)).collect(Collectors.toList());
	}

	void exampleMap(Map<String, String> map) {
		map.entrySet().stream().forEach(null);
		map.entrySet().stream().map((entry) -> entry.getKey() + entry.getValue()).collect(Collectors.toList());
		// liste dont chaque élement sera une string formattée tel que : key + value
		
		map.keySet().stream().forEach(null);
		map.values().stream().forEach(null);
	}
}
