package cpage.lesson.oop.inheritance;

public class Bag extends Product {
	public boolean withHandle;
	boolean withShoulderStrap;

	public Bag(float price, String model, String color, boolean withHandle, boolean withShoulderStrap) {
		super(price, model, color);
		this.withHandle = withHandle;
		this.withShoulderStrap = withShoulderStrap;
	}

	@Override
	public String[] getAvailableColors() {
		return new String[] { this.color, "others" };
	}
}
