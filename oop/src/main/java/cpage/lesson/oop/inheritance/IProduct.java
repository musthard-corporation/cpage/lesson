package cpage.lesson.oop.inheritance;

public interface IProduct {
	public float computeDiscount(int percentage);
	
	public String[] getAvailableColors();
}
