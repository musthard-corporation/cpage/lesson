package cpage.lesson.oop;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import cpage.lesson.oop.inheritance.Clothes;
import cpage.lesson.oop.inheritance.IProduct;
import cpage.lesson.oop.inheritance.Product;
import cpage.lesson.oop.inheritance.Bag;

public class Main {
	public static void main(String[] args) {
		Calendar birth = Calendar.getInstance();
		birth.set(1990, 7, 13);

		IdentityCard maelysGaelle = new IdentityCard("X4RTBPFW4", "MARTIN", new String[] { "Maëlys-Gaëlle", "Marie" },
				'F', "FRA", birth, "Paris",
				"44 rue DÉSIRÉ SAINT CLEMENT\nRÉSIDENCE DU PLEIN AIR BAT 4\n33000 BORDEAUX\nFRANCE", 1.68f, null);
		System.out.println(maelysGaelle.givenNames[0] + " has " + maelysGaelle.getAge() + " years old");

		// Impossible car on ne peut pas instancier une classe abstraite
		// Product other = new Product(12, "", "");

		// Impossible car on ne peut pas instancier une interface
		// IProduct other = new IProduct(12, "", "");

		int i = Integer.parseInt("truc");

		List<Product> products = new ArrayList<Product>();
		products.add(new Clothes(25, "dress", "blue", "S", 0));
		products.add(new Bag(35, "hand bag", "red", true, false));

		for (Product product : products) {
			System.out.printf(
					"The product '%s' costs %f€ and is %s%n" + "Others possible colors are : %s%n"
							+ "Currently, it costs %f€ (%d%% discount)%n%n",
					product.model, product.price, product.color, Arrays.toString(product.getAvailableColors()),
					product.computeDiscount(5), 5);

//			if (product.getClass() == Bag.class) {}
			if (product instanceof Bag) {
				Bag bag = (Bag) product;
				bag.withHandle = false;
			}
		}

		List<Bag> bags = products.stream()
				.filter(product -> product instanceof Bag)
				.map(product -> (Bag) product)
				.collect(Collectors.toList());
	}
}
