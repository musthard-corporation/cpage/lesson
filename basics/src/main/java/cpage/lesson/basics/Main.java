package cpage.lesson.basics;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
			System.out.print("Indiquez votre nom et prénom : ");
			String name = scanner.nextLine();
			System.out.println("Bonjour, " + name + " !");
			System.out.print("Indiquez votre genre : ");
			String gender = scanner.next();
			System.out.print("Indiquez votre âge : ");
			int age = scanner.nextInt();
			System.out.println("Vous êtes un(e) " + gender + " et avez " + age + " ans");
		}
    }
}

