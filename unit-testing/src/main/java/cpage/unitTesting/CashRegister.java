package cpage.unitTesting;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class CashRegister {
	/**
	 * Key : Montant de la monnaie
	 * Value : Nombre de monnaie dans la caisse enregistreuse
	 */
	private Map<Float, Integer> numberPerCashAmount = new HashMap<Float, Integer>();

	public CashRegister() {}

	public CashRegister(Map<Float, Integer> numberPerCashAmount) {
		if (numberPerCashAmount != null) {
			this.numberPerCashAmount = numberPerCashAmount;
		}
	}

	/**
	 * Ajouter un certain nombre d'une monnaie dans la caisse enregistreuse
	 * 
	 * @param cashAmount
	 * @param count
	 */
	public void addCash(float cashAmount, int count) {
		if (this.numberPerCashAmount.containsKey(cashAmount)) {
			int currentAmount = this.numberPerCashAmount.get(cashAmount);
			this.numberPerCashAmount.put(cashAmount, currentAmount + count);
		} else {
			this.numberPerCashAmount.put(cashAmount, count);
		}
	}

	/**
	 * 
	 * @param cashAmount : Montant à compter dans la caisse enregistreuse
	 * @return le nombre de monnaie pour le montant indiqué
	 */
	public float getTotalPerCash(float cashAmount) {
		return this.getTotalPerCash(this.numberPerCashAmount, cashAmount);
	}

	/**
	 * @return le montant total dans la caisse enregistreuse
	 */
	public float getTotal() {
		return this.getTotal(this.numberPerCashAmount);
	}

	/**
	 * Encaissement d'un client
	 * 
	 * @param price               : Coût total
	 * @param numberPerCashAmount : Monnaie donnée
	 * @return Montant à rendre (pour plus tard : Monnaie rendue)
	 * @throws Exception if cannot give back change (not enough money)
	 */
	public Map<Float, Integer> cashIn(float price, Map<Float, Integer> incomingCash) throws Exception {
		if (incomingCash == null) {
			throw new Exception("Pas d'argent donné");
		}
		
		float total = this.getTotal(incomingCash);
		
		if (total < price) {
			throw new Exception("La maison ne fait pas crédit");
		}

		incomingCash.forEach((key, value) -> this.addCash(key, value));

		Map<Float, Integer> change = this.giveBackChange(total - price);

		for (Entry<Float, Integer> entry : change.entrySet()) {
			float cashAmount = entry.getKey();
			int currentAmount = this.numberPerCashAmount.get(cashAmount);
			this.numberPerCashAmount.put(cashAmount, currentAmount - entry.getValue());
		}

		return change;
	}

	@Override
	public String toString() {
		return this.numberPerCashAmount.toString();
	}

	private float getTotal(Map<Float, Integer> givenCash) {
		float total = 0;

		for (float key : givenCash.keySet()) {
			total += this.getTotalPerCash(givenCash, key);
		}

		return total;
	}

	private float getTotalPerCash(Map<Float, Integer> givenCash, float cashAmount) {
		int currentAmount = givenCash.getOrDefault(cashAmount, 0);
		return currentAmount * cashAmount;
	}

	private Map<Float, Integer> giveBackChange(float price) throws Exception {
		List<Float> cashes = this.numberPerCashAmount.keySet().stream().sorted(Collections.reverseOrder())
				.collect(Collectors.toList());
		
		Map<Float, Integer> backChange = new HashMap<Float, Integer>();

		for (Float cash : cashes) {
			if (cash > price) {
				continue;
			}

			int amount = this.numberPerCashAmount.get(cash);
			int counter = 0;
			while (price >= cash && counter < amount) {
				price -= cash;
				counter++;
			}
			
			backChange.put(cash, counter);
		}
		
		if (price > 0) {
			throw new Exception("Pas assez d'argent dans la caisse");
		}

		return backChange;
	}
}
