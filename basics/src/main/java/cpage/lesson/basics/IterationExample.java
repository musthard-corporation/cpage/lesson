package cpage.lesson.basics;

@SuppressWarnings("unused")
public class IterationExample {
	private static void whileIteration(String value) {
		int index = 0;
		while (index < value.length()) {
			System.out.println(value.charAt(index));
		}
		
		while (index < value.length()) {
			if (index == 1) {
				continue;
			} else {
				System.out.println(value.charAt(index));
			}
		}

		while (true) {
			// boucle infinie
			break;
		}
	}

	private static void doWhileIteration(String value) {
		int index = 0;
		do {
			System.out.println(value.charAt(index));
		} while (index < value.length() - 1);
	}

	private static void forIteration(String value) {
		for (int index = 0; index < value.length(); index++) {
			System.out.println(value.charAt(index));
		}
		
		for (;;) {
			// boucle infinie
			break;
		}
	}

	private static void forEachIteration(String value) {
		for (char letter : value.toCharArray()) {
			System.out.println(letter);
		}
	}

	private static void streamForEachOnIteration(String value) {
		value.chars().forEach(letter -> System.out.println(letter));
	}
}
