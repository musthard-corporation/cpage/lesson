package cpage.lesson.basics;

@SuppressWarnings("unused")
public class ConditionExample {
	private static void ifElseCondition(boolean success, int value) {
		if (success) {
			System.out.println("Execution successful !");
		} else {
			System.out.println("Execution failed...");
		}

		if (value > 0) {
			System.out.println("Positive number");
		} else if (value < 0) {
			System.out.println("Negative number");
		} else {
			System.out.println("Null value");
		}
	}

	private static void switchCondition(char value) {
		switch (value) {
			case 'a':
			case 'e':
			case 'i':
			case 'o':
			case 'u':
				System.out.println("Vowel");
				break;
			case 'y':
				System.out.println("I'm not sure");
				break;
			default:
				System.out.println("Consonant");
		}
	}

	private static void ternaryCondition(boolean success, int value) {
		System.out.printf("Execution %s%n", success ? "successful !" : "failed...");
		
		System.out.printf("Number is %s%n", value % 2 == 0 ? "even" : "odd");
	}
}
