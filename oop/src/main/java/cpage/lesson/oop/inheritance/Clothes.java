package cpage.lesson.oop.inheritance;

public class Clothes extends Product {
	String size;
	int pockets;

	public Clothes(float price, String model, String color, String size, int pockets) {
		super(price, model, color);
		this.size = size;
		this.pockets = pockets;
	}

	@Override
	public String[] getAvailableColors() {
		return new String[] { this.color, "others" };
	}
}
