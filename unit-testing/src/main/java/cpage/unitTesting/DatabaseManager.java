package cpage.unitTesting;

import java.util.Arrays;
import java.util.List;

public class DatabaseManager {
	private String uri;
	
	public DatabaseManager(String uri) {
		this.uri = uri;
	}
	
	List<String> getUsers() {
		// Normalement, on fait une requête SQL
		return Arrays.asList("user1", "user2", "user3");
	}
	
	List<Integer> getAccount() {
		// Normalement, on fait une requête SQL
		return Arrays.asList(2, 8, 45);
	}
	
	void print(List<?> strings) {
		System.out.println(strings);
	}
}
