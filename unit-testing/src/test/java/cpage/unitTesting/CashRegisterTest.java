package cpage.unitTesting;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Field;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.jupiter.api.Test;

public class CashRegisterTest {
	@Test
	void constructorWithNullMap() {
		CashRegister cut = new CashRegister(null);
		assertNotNull(getNumberPerCashAmount(cut));
		assertDoesNotThrow(() -> cut.getTotal());
	}

	@Test
	void constructorWithMap() {
		Map<Float, Integer> givenMap = new HashMap<Float, Integer>();
		CashRegister cut = new CashRegister(givenMap);
		assertEquals(0f, cut.getTotal());
	}

	@Test
	void addCashSameCashMount() {
		CashRegister cut = new CashRegister();
		cut.addCash(0.01f, 1);
		cut.addCash(0.01f, 1);
		assertEquals(0.02f, cut.getTotal());
	}

	@Test
	void getTotalPerCashWithEmptyMap() {
		CashRegister cut = new CashRegister();
		float result = cut.getTotalPerCash(0.01f);
		assertEquals(0, result);
	}

	@Test
	void getTotalPerCash() {
		CashRegister cut = new CashRegister();
		cut.addCash(0.01f, 2);
		float result = cut.getTotalPerCash(0.01f);
		assertEquals(0.02f, result);
	}

	@Test
	void getTotalWithEmptyMap() {
		CashRegister cut = new CashRegister();
		float result = cut.getTotal();
		assertEquals(0, result);
	}

	@Test
	void getTotalWithNotEmptyMap() {
		CashRegister cut = new CashRegister();
		cut.addCash(0.01f, 2);
		float result = cut.getTotal();
		assertEquals(0.02f, result);
	}

	@Test
	void cashInNullIncomingCash() {
		CashRegister cut = new CashRegister();
		Exception exception = assertThrows(Exception.class, () -> cut.cashIn(0.01f, null));
		assertEquals("Pas d'argent donné", exception.getMessage());
	}

	@Test
	void cashInEmptyIncomingCash() {
		CashRegister cut = new CashRegister();
		Exception exception = assertThrows(Exception.class, () -> cut.cashIn(0.01f, Collections.emptyMap()));
		assertEquals("La maison ne fait pas crédit", exception.getMessage());
	}

	@Test
	void cashInNotEnoughtIncomingCash() {
		CashRegister cut = new CashRegister();
		Map<Float, Integer> incomingCash = new HashMap<Float, Integer>();
		incomingCash.put(0.20f, 1);
		Exception exception = assertThrows(Exception.class, () -> cut.cashIn(0.13f, incomingCash));
		assertEquals("Pas assez d'argent dans la caisse", exception.getMessage());
	}

	@Test
	void cashInNotEnoughtIncomingCashBis() {
		Map<Float, Integer> storedCash = new HashMap<Float, Integer>();
		storedCash.put(0.01f, 0);
		CashRegister cut = new CashRegister(storedCash);
		Map<Float, Integer> incomingCash = new HashMap<Float, Integer>();
		incomingCash.put(0.20f, 1);
		Exception exception = assertThrows(Exception.class, () -> cut.cashIn(0.13f, incomingCash));
		assertEquals("Pas assez d'argent dans la caisse", exception.getMessage());
	}

	@Test
	void cashIn() throws Exception {
		Map<Float, Integer> storedCash = new HashMap<Float, Integer>();
		storedCash.put(0.01f, 20);
		CashRegister cut = new CashRegister(storedCash);
		Map<Float, Integer> incomingCash = new HashMap<Float, Integer>();
		incomingCash.put(0.20f, 1);
		Map<Float, Integer> result = cut.cashIn(0.13f, incomingCash);
		assertEquals(1, result.size());
		assertTrue(result.containsKey(0.01f));
		assertEquals(13, result.get(0.01f));
	}

	@Test
	void testToString() {
		CashRegister cut = new CashRegister();
		assertEquals("{}", cut.toString());
		cut.addCash(0.01f, 2);
		assertEquals("{0.01=2}", cut.toString());
	}

	public static Map<Float, Integer> getNumberPerCashAmount(CashRegister cashRegister) {
		try {
			Field field = CashRegister.class.getDeclaredField("numberPerCashAmount");
			field.setAccessible(true);
			return (Map<Float, Integer>) field.get(cashRegister);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Collections.emptyMap();
	}
}