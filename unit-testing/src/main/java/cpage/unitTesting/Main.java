package cpage.unitTesting;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Main {

	public static void main(String[] args) {
		example1();
	}
	
	public static void example1() {
		CashRegister caisse = new CashRegister();

		caisse.addCash(5.0f, 10);
		// 5 * 10 = 50
		caisse.addCash(0.01f, 10);
		// 0.01 * 10 = 0.10
		// total = 50.10
		caisse.addCash(5.0f, 5);
		// total = 101

		System.out.println(caisse);
		// {5.0=15, 0.01=10}

		System.out.println(caisse.getTotalPerCash(5.0f));
		// 15 * 5 = 75

		System.out.println(caisse.getTotalPerCash(10.0f));
		// null * 10 = null

		System.out.println(caisse.getTotal());
		// 75.10

		Map<Float, Integer> givenCash = new HashMap<Float, Integer>();
		givenCash.put(5.0f, 2);
		givenCash.put(2.0f, 1);
		givenCash.put(0.05f, 1);

		try {
			System.out.println(caisse.cashIn(25.02f, givenCash));
			// pas de monnaie à rendre
		} catch (Exception e) {
			System.err.println("Il reste " + e.getMessage() + " à rendre mais on n'a plus de monnaie");
		}

		// il reste 50.05
		System.out.println(caisse.getTotal());
		
		try {
			System.out.println(caisse.cashIn(25.05f, givenCash));
		} catch (Exception e) {
			System.err.println("Il reste " + e.getMessage() + " à rendre mais on n'a plus de monnaie");
		}
	}

	public float example2(CashRegister caisse) {
		caisse.addCash(5f, 1);
		caisse.addCash(10f, 1);
		return caisse.getTotal();
	}
	
	public Map<String, Integer> getAccountPerUser(DatabaseManager db) {		
		List<String> users = db.getUsers();
		List<Integer> accounts = db.getAccount();
		
		db.print(users);
		db.print(accounts);
		
		Map<String, Integer> result = new HashMap<String, Integer>();
		for (int i = 0; i < users.size(); i++) {
			result.put(users.get(i), accounts.get(i));
		}
		
		return result;
	}
}
