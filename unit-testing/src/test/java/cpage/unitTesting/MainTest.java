package cpage.unitTesting;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.times;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class MainTest {

	@Test
	public void testMock() {
		DatabaseManager cut = Mockito.mock(DatabaseManager.class);
		Mockito.doReturn(Arrays.asList("u1")).when(cut).getUsers();
		Mockito.doCallRealMethod().when(cut).getAccount();
		Map<String, Integer> result = new Main().getAccountPerUser(cut);
		assertEquals(1, result.size());
		assertTrue(result.containsKey("u1"));
		assertEquals(2, result.get("u1"));
	}
	
	@Test
	public void testSpy() {
//		Espionner une classe sans l'initialiser nécessite d'avoir un constructor sans argument
//		DatabaseManager cut = Mockito.spy(DatabaseManager.class);
		DatabaseManager cut = Mockito.spy(new DatabaseManager(""));
		List<String> usersList = Arrays.asList("u1");
		Mockito.doReturn(usersList).when(cut).getUsers();
		
		// Possibilité d'utiliser any* dans le mock de méthodes
//		Mockito.doCallRealMethod().when(cut).print(usersList);
//		Mockito.doCallRealMethod().when(cut).print(anyList());
//		Mockito.doCallRealMethod().when(cut).print(any());
		
		Map<String, Integer> result = new Main().getAccountPerUser(cut);
		assertEquals(1, result.size());
		assertTrue(result.containsKey("u1"));
		assertEquals(2, result.get("u1"));
		
		// Si pas de times() précisé, times(1) par défaut
		Mockito.verify(cut).getUsers();
		Mockito.verify(cut).print(usersList);
		Mockito.verify(cut, times(2)).print(anyList());
		Mockito.verify(cut, times(2)).print(any());
		Mockito.verify(cut, times(2)).print(any(List.class));
	}

	@Test
	public void testMockBis() {
//		CashRegister cut = Mockito.mock(CashRegister.class, CALLS_REAL_METHODS);
		CashRegister cut = Mockito.mock(CashRegister.class);
		Mockito.doReturn(999f).when(cut).getTotal();
		float result = new Main().example2(cut);
		assertEquals(999f, result);
	}

	@Test
	public void testSpyBis() {
		CashRegister cut = Mockito.spy(CashRegister.class);
		Mockito.doNothing().when(cut).addCash(5f, 1);
		// Mockito.doNothing().doCallRealMethod().when(cut).addCash(any(Float.class),
		// any(Integer.class));
		float result = new Main().example2(cut);
		assertEquals(10f, result);
		Mockito.verify(cut, times(2)).addCash(any(Float.class), any(Integer.class));
	}
}
